//
//  Movie.swift
//  Movies
//
//  Created by Araceli Perez on 11/08/21.
//

import Foundation
import UIKit

struct Movie{
    let id: String
    let title: String
    let imageUrl: URL
    var image: UIImage?
    var year: String?
    var genres: String?
    var countries: String?
    var plot: String?
    var directors: String?
    
    
  /*  init(id: String, title: String, imageUrl: URL){
        self.id = id
        self.title = title
        self.imageUrl = imageUrl
    }
    
    init(id: String, title: String, imageUrl: URL, year: String, genres: String,countries: String, plot: String, directors: String ){
        self.id = id
        self.title = title
        self.imageUrl = imageUrl
        self.year = year
        self.genres = genres
        self.countries = countries
        self.plot = plot
        self.directors = directors
    }*/
}
