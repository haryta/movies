//
//  MovieDetailVC.swift
//  Movies
//
//  Created by Araceli Perez on 11/08/21.
//

import UIKit

class MovieDetailVC: UIViewController {
    
    let scrollView = UIScrollView()
    let contentView = UIView()
    let activityIndicator = UIActivityIndicatorView(style: .medium)
    let titleService = TitleService()
    var idMovie: String?
    let downloadImagesService = DownloadImagesService()

    override func viewDidLoad() {
        super.viewDidLoad()

        guard (idMovie != nil) else{
            return
        }
        
        titleService.getTitle(idMovie: idMovie!) { [weak self] results, errorMessage in
            if let movie = results {
                self?.updateData(movie: movie)
            }
            if !errorMessage.isEmpty {
                print("search error: " + errorMessage)
            }
        }

        setupScrollView()
        setupViews()
    }
    
    func updateData(movie: Movie){
        self.labelTitle.text    =  "Title: \(movie.title )"
        self.labelYear.text     = "Year: \(movie.year ?? "")"
        self.labelGenre.text    = "Genre: \(movie.genres ?? "")"
        self.labelPlot.text     = "\(movie.plot ?? "")"
        self.labelDirector.text = "Director: \(movie.directors ?? "")"
        self.labelContry.text   = "Countrie: \(movie.countries ?? "")"
        
        self.imageView.addSubview(activityIndicator)
        activityIndicator.center = self.imageView.convert(self.imageView.center, from:self.imageView.superview)
        activityIndicator.startAnimating()
        
        downloadImagesService.getImage(url: movie.imageUrl){ results, errorMessage in
            if let data = results {
                if  let image = UIImage(data: data) {
                    self.activityIndicator.stopAnimating()
                    self.imageView.image = image
                   
            }
            if !errorMessage.isEmpty {
                print("search error: " + errorMessage)
            }
            }
        }
    }
    
    func setupScrollView(){
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        contentView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
    }
    
    func setupViews(){
        
        contentView.addSubview(imageView)
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 20).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 300).isActive = true

        contentView.addSubview(labelTitle)
        labelTitle.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        labelTitle.topAnchor.constraint(equalTo: imageView.bottomAnchor,constant: 25).isActive = true
        labelTitle.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        
        contentView.addSubview(labelYear)
        labelYear.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        labelYear.topAnchor.constraint(equalTo: labelTitle.bottomAnchor, constant: 25).isActive = true
        labelYear.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        
        contentView.addSubview(labelGenre)
        labelGenre.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        labelGenre.topAnchor.constraint(equalTo: labelYear.bottomAnchor, constant: 25).isActive = true
        labelGenre.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        
        contentView.addSubview(labelContry)
        labelContry.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        labelContry.topAnchor.constraint(equalTo: labelGenre.bottomAnchor, constant: 25).isActive = true
        labelContry.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        
        contentView.addSubview(labelDirector)
        labelDirector.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        labelDirector.topAnchor.constraint(equalTo: labelContry.bottomAnchor, constant: 25).isActive = true
        labelDirector.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        
        contentView.addSubview(labelPlot)
        labelPlot.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        labelPlot.topAnchor.constraint(equalTo: labelDirector.bottomAnchor, constant: 25).isActive = true
        labelPlot.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 3/4).isActive = true
        labelPlot.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
    }
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.sizeToFit()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let labelTitle: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let labelGenre: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let labelContry: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let labelYear: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let labelPlot: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .justified
        return label
    }()
    
    let labelDirector: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
}


//
// MARK: - URL Session Delegate
//
extension MovieDetailVC: URLSessionDelegate {
  func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    DispatchQueue.main.async {
      if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
        let completionHandler = appDelegate.backgroundSessionCompletionHandler {
        appDelegate.backgroundSessionCompletionHandler = nil
        completionHandler()
      }
    }
  }
}
