//
//  SearchMoviesVC.swift
//  Movies
//
//  Created by Araceli Perez on 11/08/21.
//

import UIKit

class SearchMoviesVC: UIViewController {
    
    let searchMovieService = SearchMovieService()
    let downloadImagesService = DownloadImagesService()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var movies: [Movie] = []
    lazy var tapRecognizer: UITapGestureRecognizer = {
      var recognizer = UITapGestureRecognizer(target:self, action: #selector(dismissKeyboard))
      return recognizer
    }()
    
    @objc func dismissKeyboard() {
      searchBar.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.prefetchDataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      guard
        segue.identifier == "ShowDetailMovieSegue",
        let indexPath = tableView.indexPathForSelectedRow,
        let movieDetailVC = segue.destination as? MovieDetailVC
        else {
          return
      }
      movieDetailVC.idMovie = movies[indexPath.row].id
    }
    
    func getImage(forItemAtIndex index: Int){
        let url = movies[index].imageUrl
        downloadImagesService.getImage(url: url){ results, errorMessage in
            if let data = results {
                if  let image = UIImage(data: data) {
                    if index >= self.movies.count {return}
                    self.movies[index].image = image
                    let indexPath = IndexPath(row: index, section: 0)
                    DispatchQueue.main.async {
                    if self.tableView.indexPathsForVisibleRows?.contains(indexPath) ?? false {
                        self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                    }
                }
            }
            if !errorMessage.isEmpty {
                print("search error: " + errorMessage)
            }
        }
        }
    
    }
    
    func cancelDownloadingImage(forItemAtIndex index: Int){
        let url = movies[index].imageUrl
        downloadImagesService.cancelDownloadingImage(url: url)
    }
}

//
// MARK: - Table View Data Source
//
extension SearchMoviesVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let movieCell = tableView.dequeueReusableCell(withIdentifier: MovieCell.identifier, for: indexPath) as! MovieCell
        
    let movie = movies[indexPath.row]
    
    if let imageView = movieCell.imageMovieView {
        imageView.image = nil

        if let image = movies[indexPath.row].image {
                imageView.image = image
        } else {
            let activityIndicator = UIActivityIndicatorView(style: .medium)
            imageView.addSubview(activityIndicator)
            activityIndicator.center = imageView.convert(imageView.center, from:imageView.superview)
            activityIndicator.startAnimating()
            self.getImage(forItemAtIndex: indexPath.row)
        }

    }
    
    if let labelTitle = movieCell.titleMovieLabel {
        labelTitle.text = movie.title
    }
    
    movieCell.accessoryType = .disclosureIndicator
    
    return movieCell
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return  movies.count
  }
}


//
// MARK: - Table View Delegate
//
extension SearchMoviesVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 100.0
  }
}

//
// MARK: - Search Bar Delegate
//
extension SearchMoviesVC: UISearchBarDelegate {
    
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty{
            perform(#selector(getSearch(searchText:)), with: searchText, afterDelay: 0.5)
        }
    }
    
    @objc func getSearch(searchText: String){
        searchMovieService.getSearchResults(searchMovie: searchText) { results, errorMessage in
            if let results = results {
                self.movies = results
                self.tableView.reloadData()
                self.tableView.setContentOffset(CGPoint.zero, animated: false)
            }
            if !errorMessage.isEmpty {
                print("search error: " + errorMessage)
            }
        }
    }
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    view.addGestureRecognizer(tapRecognizer)
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    view.removeGestureRecognizer(tapRecognizer)
  }
}

// MARK: - UITableViewDataSourcePrefetching
extension SearchMoviesVC: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        print("prefetchRowsAt \(indexPaths)")
        indexPaths.forEach { self.getImage(forItemAtIndex: $0.row) }

    }

    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        print("cancelPrefetchingForRowsAt \(indexPaths)")
        indexPaths.forEach { self.cancelDownloadingImage(forItemAtIndex: $0.row) }
    }
}


//
// MARK: - URL Session Delegate
//
extension SearchMoviesVC: URLSessionDelegate {
  func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    DispatchQueue.main.async {
      if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
        let completionHandler = appDelegate.backgroundSessionCompletionHandler {
        appDelegate.backgroundSessionCompletionHandler = nil
        completionHandler()
      }
    }
  }
}

