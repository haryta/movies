//
//  TitleService.swift
//  Movies
//
//  Created by Araceli Perez on 12/08/21.
//

import Foundation

class TitleService{
    
    let urlSession = URLSession(configuration: .default)
    var movie : Movie?
    var dataTask: URLSessionDataTask?
    var errorMessage = ""
    
    typealias JSONDictionary = [String: Any]
    typealias QueryResult = (Movie?, String) -> Void
    
    func getTitle(idMovie: String, completion: @escaping QueryResult) {

      //Canceling previous tasks
      dataTask?.cancel()
        guard let apiKey = Bundle.main.object(forInfoDictionaryKey: "ApiKey") as? String , let urlApi = Bundle.main.object(forInfoDictionaryKey: "URLApi") as? String else {
            return
        }
        
        if let url = URL(string: "\(urlApi)Title/\(apiKey)/\(idMovie)"){
        dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
          defer {
            self?.dataTask = nil
          }
          
          if let error = error {
            self?.errorMessage += "DataTask: " + error.localizedDescription + "\n"
          } else if
            let data = data,
            let response = response as? HTTPURLResponse,
            response.statusCode == 200 {
            
            self?.updateResult(data)
            
            DispatchQueue.main.async {
              completion(self?.movie, self?.errorMessage ?? "")
            }
          }
        }
        dataTask?.resume()
      }
    }
    
    private func updateResult(_ data: Data) {

      var movieDictionary: JSONDictionary?
      do {
        movieDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary
      } catch let parseError as NSError {
        errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
        return
      }

      let id = movieDictionary!["id"] as! String
      let imageURLString = movieDictionary!["image"] as? String
      let imageURL = URL(string: imageURLString!)
      let title = movieDictionary!["title"] as? String
      let year = movieDictionary!["year"] as? String
      let genres = movieDictionary!["genres"] as? String
      let countries = movieDictionary!["countries"] as? String
      let plot = movieDictionary!["plot"] as? String
      let directors = movieDictionary!["directors"] as? String
        
      movie = Movie(id: id, title: title ?? "", imageUrl: imageURL!, year: year ?? "", genres: genres ?? "", countries: countries ?? "", plot: plot ?? "", directors: directors ?? "")
    }
}
