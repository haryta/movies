//
//  SearchMovieService.swift
//  Movies
//
//  Created by Araceli Perez on 11/08/21.
//

import Foundation

class SearchMovieService{
    
    let urlSession = URLSession(configuration: .default)
    var movies : [Movie] = []
    var dataTask: URLSessionDataTask?
    var errorMessage = ""
    
    typealias JSONDictionary = [String: Any]
    typealias QueryResult = ([Movie]?, String) -> Void
    
    func getSearchResults(searchMovie: String, completion: @escaping QueryResult) {
      //Canceling previous tasks
      dataTask?.cancel()
                
        guard let apiKey = Bundle.main.object(forInfoDictionaryKey: "ApiKey") as? String , let urlApi = Bundle.main.object(forInfoDictionaryKey: "URLApi") as? String else {
            return
        }
        
        if let url = URL(string: "\(urlApi)SearchMovie/\(apiKey)/\(searchMovie)"){
      
        dataTask = urlSession.dataTask(with: url) { [weak self] data, response, error in
          defer {
            self?.dataTask = nil
          }
          
          if let error = error {
            self?.errorMessage += "DataTask: " + error.localizedDescription + "\n"
          } else if
            let data = data,
            let response = response as? HTTPURLResponse,
            response.statusCode == 200 {
            
            self?.updateSearchResults(data)
            
            DispatchQueue.main.async {
              completion(self?.movies, self?.errorMessage ?? "")
            }
          }
        }
        dataTask?.resume()
      }
    }
    
    private func updateSearchResults(_ data: Data) {
      var response: JSONDictionary?
      movies.removeAll()
      do {
        response = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary
      } catch let parseError as NSError {
        errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
        return
      }
      
      guard let array = response!["results"] as? [Any] else {
        errorMessage += "Dictionary does not contain results key\n"
        return
      }
    
      for movieDictionary in array {
        if let movieDictionary = movieDictionary as? JSONDictionary,
          let id = movieDictionary["id"] as? String,
          let imageURLString = movieDictionary["image"] as? String,
          let imageURL = URL(string: imageURLString),
          let title = movieDictionary["title"] as? String {
            let movie = Movie(id: id, title: title, imageUrl: imageURL)
            movies.append(movie)
        } else {
          errorMessage += "Error parsing movieDictionary\n"
        }
      }
    }
    
}


