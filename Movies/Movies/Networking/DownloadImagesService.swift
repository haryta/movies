//
//  DownloadImagesService.swift
//  Movies
//
//  Created by Araceli Perez on 12/08/21.
//

import Foundation


class DownloadImagesService{
    let urlSession = URLSession(configuration: .default)
    var tasks = [URLSessionTask]()
    typealias QueryResult = (Data?, String) -> Void
    var errorMessage = ""
    var dataTask: URLSessionDataTask?

    func getImage(url: URL, completion: @escaping QueryResult) {
        guard tasks.firstIndex(where: { $0.originalRequest?.url == url }) == nil else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                completion(data, self.errorMessage )
            }
        }
        
        task.resume()
        tasks.append(task)
    }
    
    func cancelDownloadingImage(url: URL ) {
        guard let taskIndex = tasks.firstIndex(where: { $0.originalRequest?.url == url }) else {
            return
        }
        let task = tasks[taskIndex]
        task.cancel()
        tasks.remove(at: taskIndex)
    }
}
