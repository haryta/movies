//
//  MovieCell.swift
//  Movies
//
//  Created by Araceli Perez on 12/08/21.
//

import UIKit

class MovieCell: UITableViewCell {
    static let identifier = "movieCell"
    
    @IBOutlet weak var titleMovieLabel: UILabel!
    @IBOutlet weak var imageMovieView: UIImageView!
    
    override func prepareForReuse() {
      super.prepareForReuse()
        titleMovieLabel.text = nil
        imageMovieView.image = nil
    }
}



